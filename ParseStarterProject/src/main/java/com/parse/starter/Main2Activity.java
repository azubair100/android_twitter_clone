package com.parse.starter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    ArrayList<String> users = new ArrayList<>();

    ArrayAdapter arrayAdapter ;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.tweet_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //IF YOU WANT TO CREATE A NEW TWEET
        if(item.getItemId() == R.id.tweet) {

            //SETTING UP MODIFIED ALERT
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Send a Tweet");

            final EditText tweetContentEditText = new EditText(this);

            builder.setView(tweetContentEditText);

            builder.setPositiveButton("Send",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Log.i("Info", tweetContentEditText.getText().toString());

                            ParseObject tweet = new ParseObject("Tweet");

                            tweet.put("username", ParseUser.getCurrentUser().getUsername());

                            tweet.put("tweet", tweetContentEditText.getText().toString());

                            tweet.saveInBackground(
                                    new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {

                                            if(e== null){

                                                Toast.makeText(Main2Activity.this, "Tweet Sent", Toast.LENGTH_LONG).show();
                                            }

                                            else{

                                                Toast.makeText(Main2Activity.this, "Tweet Was Not Sent", Toast.LENGTH_LONG).show();

                                            }

                                        }
                                    }
                            );

                        }
                    }
            );

            builder.setNegativeButton("Cancel",

                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();

                        }
                    }
            );

            builder.show();

        }

        //IF THE USER WANTS TO LOG OUT
        else if(item.getItemId() == R.id.logout) {

            ParseUser.logOut();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);

        }

        //USER WANTS TO SEE THEIR OWN FEED
        else if(item.getItemId() == R.id.viewFeed) {

            Intent intent = new Intent(getApplicationContext(), Feed.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        setTitle("User List");
        users.add("Rob");
        users.add("Mob");

        //TO STORE AN ARRAY OF OPTIONS IN SIDE THE PARSE SERVER DATABASE.. YOU NEED TO INITIALIZE IT INSIDE THE CODE
        //SAME WAY WHEN YOU DECLARE AN ARRAY
        if(ParseUser.getCurrentUser().get("isFollowing") == null){

            List<String> emptyList = new ArrayList<>();

            ParseUser.getCurrentUser().put("isFollowing", emptyList);

        }

        final ListView listView = (ListView) findViewById(R.id.listView);

        //THE ABILITY TO CHOOSE AS MANY PEOPLE AS THEY LIKE
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

        //THE TICK AT THE END OF EACH ROWM WHCIH THE USER CAN SELECT AND DE SELECT
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_checked, users);

        listView.setAdapter(arrayAdapter);

        //NOW WE HAVE TO ADD AN ITEM CLICK LISTENER, SO WHEN EACH ROW IS CLICKED WE CAN CHOOSE WHAT TO DO

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        CheckedTextView checkedTextView = (CheckedTextView) view; //THE VIEW IS IN THE PARAMS

                        if(checkedTextView.isChecked()){

                            Log.i("Row checked at",  parent.toString());

                            //STORE ARRAY OF INFORMATION IN THE PARSE SERVER, IF SELECTED
                            ParseUser.getCurrentUser().getList("isFollowing").add(users.get(position));

                            ParseUser.getCurrentUser().saveInBackground();

                        }

                        else{

                            //IF THE USER UNFOLLOWS SOMEONE
                            ParseUser.getCurrentUser().getList("isFollowing").remove(users.get(position));
                            Log.i("Row", "Not Checked");

                        }

                    }
                }
        );


        //CLEAR ALL PREVIOUS USERS INSIDE THE ARRAYLSIT IF THEY HAVE ANYTHIGN SAVED INIT FORM BEFORE
        users.clear();

        ParseQuery<ParseUser> query = ParseUser.getQuery();

        //DISPLAY ALL USERS EXCEPT FOR THE CURRENT USER
        query.whereNotEqualTo("username", ParseUser.getCurrentUser().getUsername());

        query.findInBackground(
                new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        if(e== null){

                            if(objects.size() > 0){

                                for(ParseUser user: objects){

                                    users.add(user.getUsername());

                                }

                                arrayAdapter.notifyDataSetChanged();

                                //ON THE USER'S FEED YOU WOULD WANT TO SHOW THE USERS THAT WERE FOLLOWED BY THE CURRENT USER
                                for(String username :users){
                                    //THIS CHECKS WITH IF THE INDEX USERNAME IS INSIDE THE CURRENT USER'S FOLLOWING ARRAY
                                    if(ParseUser.getCurrentUser().getList("isFollowing").contains(username)){

                                        listView.setItemChecked(users.indexOf(username), true);

                                    }

                                }


                            }

                        }

                    }
                }
        );


    }
}
