package com.parse.starter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Feed extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        setTitle("Your Feed");

        final ListView feedListView = (ListView) findViewById(R.id.feedListView);
/*
        //MAPS ARE LIKE HASHES IN RUBY
        List<Map<String, String>> tweetData = new ArrayList<>();

        //CREATING HARD CODED TWEETS
        //SO THIS MAP CURRENTLY HAS 5 MAPS
        //WHERE EACH TWEET INFO HAS A USERNAME AND CONTENT

        for(int i = 1; i <= 5; i++){

            Map<String, String> tweetInfo = new HashMap<>();
            tweetInfo.put("content", "Tweet Content " + Integer.toString(i));

            tweetInfo.put("username", "User " + Integer.toString(i));

            tweetData.add(tweetInfo);
        }

    */
        //GET DATA FROM THE PARSE SERVER

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tweet");

        //WHERE THE USERNAME IS CONTAINED IN A LIST OF USERS THAT OUR USER IS FOLLOWING
        query.whereContainedIn("username", ParseUser.getCurrentUser().getList("isFollowing"));
        query.orderByDescending("createdAt");
        query.setLimit(20);

        query.findInBackground(
                new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {

                        if(e == null){

                            if(objects.size() > 0){

                                List<Map<String, String>> tweetData = new ArrayList<>();

                                for(ParseObject tweet : objects){

                                    //EACH OF THIS WE WANT TO USE OUR TWEET DATA HERE


                                        Map<String, String> tweetInfo = new HashMap<>();
                                        tweetInfo.put("content", tweet.getString("tweet"));

                                        tweetInfo.put("username", tweet.getString("username"));

                                        tweetData.add(tweetInfo);
                                }

                                //SIMPLE ADAPTER
                                SimpleAdapter simpleAdapter = new SimpleAdapter(
                                        Feed.this,
                                        tweetData, //THE HASH MAP LIST
                                        android.R.layout.simple_list_item_2, //ITEM 2 USED FOR CONTENT 1 AND CONTENT 2
                                        new String[] {"content", "username"}, //THE ATTRIBUTES OF THE OBJECTS THAT WE ARE GETTING THE DATA FROM
                                        new int[] {android.R.id.text1, android.R.id.text2} //WHERE DO WE WANT THOSE INFORMATION TO GO, ITEM1 AND SUB ITEM 1
                                );

                                feedListView.setAdapter(simpleAdapter);

                                }

                            }

                        }

                    });
                }










    }

