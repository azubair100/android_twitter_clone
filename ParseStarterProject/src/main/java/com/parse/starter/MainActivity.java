/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    //IN THIS METHOD WE CHECK IF THE USER IS LOGGED IN OR NOT
    public void redirectUSer(){

        if(ParseUser.getCurrentUser() != null){

            Intent intent = new Intent(getApplicationContext(), Main2Activity.class);

            startActivity(intent);

        }

    }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    setTitle("Twitter : Login");

      redirectUSer();
    ParseAnalytics.trackAppOpenedInBackground(getIntent());
  }


  //IF THE USER TRIES TO LOG IN BUT MIS TYPES THEIR USER NAME, THEN INSTEAD OF GIVING THEM AN ERROR MESSAGE,
  // IT WILL CREATE A NEW ACCOUNT FOR THEM WITH A NEW USERNAME
  public void signUpLogin(View view) {

    final EditText userNameEditText = (EditText) findViewById(R.id.userNameEditText);
    final EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);

    //ATTEMPT TO LOGIN FIRST

    ParseUser.logInInBackground(
            userNameEditText.getText().toString(),
            passwordEditText.getText().toString(),
            new LogInCallback() {
              @Override
              public void done(ParseUser user, ParseException e) {

                if(e == null){

                  Log.i("Logged In", "True");

                    redirectUSer();

                }
                //ELSE SIGN THE USER UP
                else{

                  ParseUser parseUser = new ParseUser();

                  parseUser.setUsername(userNameEditText.getText().toString());
                  parseUser.setPassword(passwordEditText.getText().toString());

                  parseUser.signUpInBackground(
                          new SignUpCallback() {
                            @Override
                            public void done(ParseException e) {

                              if(e == null){

                                Log.i("Sign UP", "Successful");

                                  redirectUSer();

                              }
                              //SIGN UP FAILED
                              else {

                                //e.getMessage().substring(e.getMessage().indexOf(" "))
                                //THIS CODE IS ONLY FOR CUTTING OF THE NASTY JAVALNGPOINTEREXCEPTION ERROR THING
                                Toast.makeText(MainActivity.this, e.getMessage().substring(e.getMessage().indexOf(" ")), Toast.LENGTH_LONG).show();
                              }

                            }
                          }
                  );

                }

              }
            }
    );

  }
}